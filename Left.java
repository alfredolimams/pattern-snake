
public class Left implements Order {
	@Override
	public Tuple execute(Tuple head) {
		return head.left(head);
	}
}
