
public class Up implements Order {
	@Override
	public Tuple execute(Tuple head) {
		return head.up(head);
	}
}
