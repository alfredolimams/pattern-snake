
public class BuilderDrive extends CommandsBuilder {

	public void build() {
		commands.add(null);
		commands.add(new Right());
		commands.add(new Left());
		commands.add(new Down());
		commands.add(new Up());
	}
}
