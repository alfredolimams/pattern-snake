
public interface Order {
	   Tuple execute( Tuple head );
}