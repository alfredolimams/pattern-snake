
public class Right implements Order {
	@Override
	public Tuple execute(Tuple head) {
		return head.right(head);
	}
}
