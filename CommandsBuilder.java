
public abstract class CommandsBuilder {
	protected Commands commands = new Commands();

	public CommandsBuilder() {
	}

	public Commands getCommand() {
		return commands;
	}

	public abstract void build();
}
