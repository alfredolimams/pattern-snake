
public class BuilderKeys extends CommandsBuilder {

	@Override
	public void build() {
	}

	public void buildLeft(){
		
		commands.add( null );
		commands.add( new KeyLeft() );
		commands.add( new KeyUp() );
		commands.add( new KeyLeft() );
		commands.add( new KeyDown() );
		
	}

	public void buildRight(){
		
		commands.add( null );
		commands.add( new KeyRight() );
		commands.add( new KeyUp() );
		commands.add( new KeyRight() );
		commands.add( new KeyDown() );		
	}	

	public void buildUp(){
		
		commands.add( null );
		commands.add( new KeyLeft() );
		commands.add( new KeyUp() );
		commands.add( new KeyRight() );
		commands.add( new KeyUp() );
	}
	
	public void buildDown(){
		
		commands.add( null );
		commands.add( new KeyLeft() );
		commands.add( new KeyDown() );
		commands.add( new KeyRight() );
		commands.add( new KeyDown() );
		
	}
	
}
