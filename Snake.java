import java.util.LinkedList;

public class Snake {

	LinkedList<Tuple> positions;
	Commands cmd;

	public Snake() {
		positions = new LinkedList<Tuple>();
		CommandsBuilder command = new BuilderDrive();
		command.build();
		cmd = command.getCommand();
		positions.addFirst(new Tuple(0, 2));
		positions.addFirst(new Tuple(1, 2));
		positions.addFirst(new Tuple(2, 2));
	}

	public int size() {
		return positions.size();
	}

	public void move(int dir) {
		positions.addFirst(cmd.move(headSnake(), dir));
	}

	public boolean eat(Tuple food) {
		if (headSnake().equals(food)) {
			positions.addLast(positions.getLast());
			return true;
		}
		return false;
	}

	public Tuple headSnake() {
		return positions.getFirst();
	}

	public Tuple tailSnake() {
		return positions.getLast();
	}

	public Tuple deleteTailSnake() {
		return positions.removeLast();
	}

	public boolean have(Tuple pos) {
		for (Tuple p : positions) {
			if (pos.equals(p)) {
				return true;
			}
		}
		return false;
	}

	public boolean checkCollision() {
		Collect list = new Collect( positions );
		Tuple head = (Tuple)list.next();
		while( list.hasNext() )	{
			if( head.equals( (Tuple)list.next() ) )
				return true;
		}
		return false;
	}

	public LinkedList<Tuple> snake() {
		return positions;
	}

	public Tuple getValAleaNotInSnake() {
		Tuple p = new Tuple();
		if (have(p)) {
			return getValAleaNotInSnake();
		}
		return p;
	}
	
}
