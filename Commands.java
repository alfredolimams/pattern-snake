import java.util.ArrayList;

public class Commands {
	private ArrayList<Object> commands = new ArrayList<Object>();

	public Commands() {
	}

	public Tuple move(Tuple head, int dir) {
		return ((Order) commands.get(dir)).execute(head);
	}
	
	public int direction(int dir) {
		return ((Read) commands.get(dir)).execute();
	}

	void add(Object obj) {
		commands.add(obj);
	}
	void resize( int size ){
		commands.ensureCapacity(size);
	}
	
}
